package com.kutylo.model.States.impl;

import com.kutylo.model.States.State;
import com.kutylo.model.Task;

public class ToDoState extends State {

    @Override
    public void toProgress(Task task) {
        task.setState(new InProgressState());
    }

}
