package com.kutylo.model.States.impl;

import com.kutylo.model.States.State;
import com.kutylo.model.Task;

public class CodeReviewState extends State {
    @Override
    public void addToDo(Task task) {
        task.setState(new ToDoState());
    }

    @Override
    public void toProgress(Task task) {
        task.setState(new InProgressState());
    }

}
