package com.kutylo.model.States.impl;

import com.kutylo.model.States.State;
import com.kutylo.model.Task;

public class InProgressState extends State {
    @Override
    public void toReview(Task task) {
        task.setState(new CodeReviewState());
    }
}
