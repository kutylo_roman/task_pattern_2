package com.kutylo.model;

import java.util.ArrayList;

public class Tasks {
    private ArrayList<Task> tasks;

    public Tasks() {
        tasks = new ArrayList<>();
    }

    public Task getTask(int index) {
        return tasks.get(index);
    }

    public void addTask(String taskName) {
        tasks.add(new Task(taskName));
    }

    @Override
    public String toString() {
        return "\n" + tasks.toString();
    }
}
