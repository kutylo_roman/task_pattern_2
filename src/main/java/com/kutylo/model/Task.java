package com.kutylo.model;

import com.kutylo.model.States.State;
import com.kutylo.model.States.impl.ToDoState;

import java.util.ArrayList;
import java.util.List;

public class Task {
    private static int counter = 0;
    private int taskNum;
    private State state;
    private String TaskName;

    private List<User> users;

    public Task(String taskName) {
        counter++;
        taskNum = counter;
        this.TaskName = taskName;
        state = new ToDoState();
        users = new ArrayList<>();
    }


    public int getTaskNum() {
        return taskNum;
    }


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String taskName) {
        TaskName = taskName;
    }

    public boolean addUser(User user) {
        return users.add(user);
    }

    public void removeUser(String username) {
        users.forEach(user -> {
            if (user.getName().equals(username)) users.remove(user);
        });
    }

    @Override
    public String toString() {
        return "\nNumber=" + taskNum +
                ", State=" + state +
                ", Users: {" + users + "}" +
                ", Task='" + TaskName + "'";
    }
}
