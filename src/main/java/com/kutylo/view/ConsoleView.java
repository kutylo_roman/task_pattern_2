package com.kutylo.view;

import com.kutylo.controller.Controller;
import com.kutylo.model.Task;
import com.kutylo.model.States.impl.CodeReviewState;
import com.kutylo.model.States.impl.DoneState;
import com.kutylo.model.States.impl.InProgressState;
import com.kutylo.model.States.impl.ToDoState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class ConsoleView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Class> states;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Add new Task.");
        menu.put("2", "2 - Change task state.");
        menu.put("3", "3 - Get information about current tasks.");
        menu.put("4", "4 - Add user to task.");
        menu.put("5", "5 - Remove user from task.");
        menu.put("Q", "Q");
    }

    private void setStates() {
        states = new LinkedHashMap<>();
        states.put("1", ToDoState.class);
        states.put("2", InProgressState.class);
        states.put("3", CodeReviewState.class);
        states.put("4", DoneState.class);
    }

    public ConsoleView() {
        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        setStates();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::addNewTask);
        methodsMenu.put("2", this::changeState);
        methodsMenu.put("3", this::getInfo);
        methodsMenu.put("4", this::addUser);
        methodsMenu.put("5", this::removeUser);
    }

    private void removeUser() {
        logger.info("Enter task number to change: ");
        Scanner scanner = new Scanner(System.in);
        int taskNumber = scanner.nextInt();
        logger.info("Enter name to remove: ");
        scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        controller.removeUser(userName, taskNumber - 1);
    }

    private void addUser() {

        logger.info("Enter task number to change: ");
        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();

        logger.info("Enter name to add: ");
        scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        controller.addUser(userName, index - 1);
    }

    private void changeState() {


        logger.info("Enter task number to change: ");
        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();
        Task task = controller.getTask(index - 1);

        printStates();

        logger.info("Enter state number: ");
        scanner = new Scanner(System.in);
        index = scanner.nextInt();

        switch (index) {
            case 1:
                task.getState().addToDo(task);
                break;
            case 2:
                task.getState().toProgress(task);
                break;
            case 3:
                task.getState().toReview(task);
                break;
            case 4:
                task.getState().toDone(task);
                break;
            default:
                logger.error("Wrong input");
                break;
        }

    }

    private void addNewTask() {
        logger.info("Enter task to add: ");
        Scanner scanner = new Scanner(System.in);
        String task = scanner.nextLine();
        controller.addTask(task);
    }

    private void getInfo() {
        logger.info("Current tasks: \n");
        logger.info(controller.getInfo());
    }

    private void printStates() {
        logger.info(states.toString());
    }

    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
