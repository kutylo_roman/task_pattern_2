package com.kutylo.controller;

import com.kutylo.model.States.State;
import com.kutylo.model.Task;
import com.kutylo.model.Tasks;
import com.kutylo.model.User;

public class Controller {

    Tasks tasks;

    public Controller() {
        tasks = new Tasks();
    }

    public void addTask(String taskName) {
        tasks.addTask(taskName);
    }

    public Task getTask(int index) {
        return tasks.getTask(index);
    }

    public void setTaskState(int index, State state) {
        tasks.getTask(index).setState(state);
    }

    public String getInfo() {
        return tasks.toString();
    }

    public void addUser(String name, int index) {
        Task task = getTask(index);
        task.addUser(new User(name));
    }

    public void removeUser(String name, int index) {
        Task task = getTask(index);
        task.removeUser(name);
    }
}
